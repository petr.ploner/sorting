var default_array = [18, 2, 8, 26, 20, 47, 12, 8, 16, 90, 3, 1];

/***********************************************************************************************************************/

/*                                               METODY PŘÍMÉHO TŘÍDĚNÍ                                                *
/**********************************************************************************************************************/

/**
 * V každém kroku se vezme první prvek z nesetříděné části
 * a projde se již setříděná část a zařadí se na správné místo
 */
function insertionSort() {
    var array = default_array;

    for (i = 1; i < array.length; i++) { //1. prvek se bere jako setříděná část
        var x = array[i];
        var j = i - 1; //nastaví se index na konec setříděné části

        while (j >= 0 && array[j] > x) { //a pole se odzadu projíždí a porovnává se aktuální prvek x
            showCompared(x, array[j]);
            array[j + 1] = array[j];
            j--;
        }

        array[j + 1] = x;

        printArray(array);
    }
}

/**
 * Vždy porovnáváme dvě sousední hodnoty, pokud je levá větší než pravá, tak je
 * vyměníme. V každém průběhu tak zajistíme, že největší hodnota "probublá" nakonec pole
 */
function bubbleSort() {
    var array = default_array;

    for (var i = array.length - 1; i >= 0; i--) { //počítáme indexy od konce, protože setřiďovaná část se s každým průběhem zmenšuje
        for (var j = 0; j < i; j++) { //vždy jedeme do i, jelikož na dalších pozicích už jsou nevětší tj. setříděné hodnoty
            showCompared(array[j], array[j + 1]);
            if (array[j] > array[j + 1]) {
                array = exchange(array, j, j + 1);
            }
            printArray(array);
        }
    }

    printArray(array);
}

/**
 * Pole se rozdělí na dvě části - setříděná a nesetříděná, kdy se v každém průchodu hledá nejmenší prvek v nesetříděné
 * části a ten se dosadí na své místo v části setříděné.
 */
function selectionSort() {
    var array = default_array;

    var min;

    for (i = 0; i < array.length; i++) {
        min = i; //nejmenší prvek je na počátku nesetříděné části
        for (j = i + 1; j < array.length; j++) {
            showCompared(array[j], array[min]);
            if (array[j] < array[min]) {
                min = j; //pokud jsme našli ještě menší prvek, než array[i], tak se uloží jeho index
            }
        }

        if (i !== min) {
            array = exchange(array, i, min); //pokud není prvek array[i] nejmenší, tak se provede výměna prvku array[i] a array[min]
        }

        printArray(array);
    }

    printArray(array);
}

/***********************************************************************************************************************/

/*                                               METODY NEPŘÍMÉHO TŘÍDĚNÍ                                              *
/**********************************************************************************************************************/

/**
 * Efektivní metoda třídění výměnou
 *
 */
function quickSort() {
    var array = default_array;
    quickSortAlgPivotInTheMiddleVecerka(array, 0, array.length - 1); //jako počáteční krok nastavíme hranici celého pole
}

/**
 *
 * @param array
 * @param left hranice
 * @param right hranice
 */
function quickSortAlgPivotInTheMiddleVecerka(array, left, right) {
    var pivot = array[Math.round((left + right) / 2)]; //volba pivota uprostřed pole
    console.warn("PIVOT: " + pivot + " hranice: " + left + " | " + right);

    var i = left;
    var j = right;

    do {
        while (array[i] < pivot) { //procházíme pole dozadu a hledáme prvek, který je >= pivotovi
            i++;
        }

        while (pivot < array[j]) { //procházíme pole odzadu a hledáme prvek, který je <= pivotovi
            j--;
        }

        if (i > j) { //pole je v tuto chvíli rozděleno na dvě části
            break;
        }

        console.error("Prohazuji: " + array[i] + " | " + array[j]);
        array = exchange(array, i, j); //prohodí se nalezené prvky tak, aby v levé části byly menší než pivot a v pravé větší než pivot

        i++;
        j--;

        printArray(array);

    } while (i <= j);

    if (left < j) {
        quickSortAlgPivotInTheMiddleVecerka(array, left, j); //pokud je v levé části více než jeden prvek tj. levá hranice je meší než j tak provedeme setřídění této části
    }

    if (i < right) {
        quickSortAlgPivotInTheMiddleVecerka(array, i, right);//pokud je v pravé části více než jeden prvek tj. pravá hranice je větší než i, tak provedeme setřídění této části
    }
}


function heapSort() {
    
}

/***********************************************************************************************************************/

/*                                                      HELPERY                                                        *
/**********************************************************************************************************************/

function exchange(array, i, j) {
    var tmp = array[i];
    array[i] = array[j];
    array[j] = tmp;

    return array;
}

/**
 * @deprecated //pouze pro to, aby v kódu bylo škrtnuté
 */
function printArray(array) {
    var result = "";
    for (var i = 0; i < array.length; i++) {
        result += i > 0 ? "," + array[i] : array[i];
    }

    console.log(result);
}

/**
 * @deprecated //pouze pro to, aby v kódu bylo škrtnuté
 */
function showCompared(x, y) {
    console.warn(x + " vs " + y);
}